import math
import sys
import numpy as np
import pylab as p

#Author: Ross Koval

def euler_spring(v0, x0, k, m, dt, tmax):
    t = 0
    t_s = np.linspace(0, 13.35, float(tmax)/dt)
    v = v0
    v_s = [v0]
    x = x0
    x_s = [x0]
    a0 = -float(k) * x0 / m
    a = a0
    a_s =[-float(k) * x0 / m]
    while len(v_s) < len(t_s):
        x += v * dt
        v += a * dt
        v_s.append(v)
        x_s.append(x)
        a = -float(k) * x / m
        a_s.append(a)
    return np.asarray(t_s), np.asarray(x_s), np.asarray(v_s), np.asarray(a_s)

def exact_spring(k, m, dt, tmax):
    c = np.sqrt(2.0/3.0)
    t_s = np.linspace(0, tmax, float(tmax)/dt)
    x_s = np.cos(c*t_s) + np.sin(c*t_s)
    v_s = -c*np.sin(c*t_s) + c*np.cos(c*t_s)
    return t_s, x_s, v_s

def graph(x, y, title, ylabel):
     p.xlabel("Time (s)")
     p.ylabel(ylabel)
     p.title(title)
     p.plot(x, y)
     
def euler_spring_implicit(v0, x0, k, m, dt, tmax):
    t = 0
    t_s = np.linspace(0, 13.35, float(tmax)/dt)
    v = v0
    v_s = [v0]
    x = x0
    x_s = [x0]
    a0 = -float(k) * x0 / m
    a = a0
    a_s =[-float(k) * x0 / m]
    while len(v_s) < len(t_s):
        x_new = (x + v*dt)/(1+dt**2)
        v = (v-x*dt)/(1+dt**2) 
        x = x_new
        v_s.append(v)
        x_s.append(x)
        a = -float(k) * x / m
        a_s.append(a)
    return np.asarray(t_s), np.asarray(x_s), np.asarray(v_s), np.asarray(a_s)
 
h = 0.1  
euler = euler_spring(1, 1, 1, 1, h, 13.35)
euler_im = euler_spring_implicit(1, 1, 1, 1, h, 13.35)
exact = exact_spring(1, 1, h, 13.35)   
graph(euler[0], euler[1], "Position vs. Time using Euler's method", "Position (m)")
p.savefig("euler_position.png")
#p.show()
graph(exact[0], exact[1], "Position vs. Time using exact solution", "Position (m)")
p.savefig("exact_position.png")
#p.show()
graph(euler[0], euler[2], "Velocity vs. Time using Euler's method", "Velocity (m/s)")
p.savefig("euler_velocity.png")
#p.show()
graph(exact[0], exact[2], "Velocity vs. Time using exact solution", "Velocity (m/s")
p.savefig("exact_velocity.png")
#p.show()

# Error as a function of time
position_error = exact[1] - euler[1]
velocity_error = exact[2] - euler[2]
p.title("Position error using explicit")
p.xlabel("Time (s)")
p.ylabel("Error (m)") 
p.plot(euler[0], position_error)
p.savefig("position_error.png")
#p.show()
p.title("Velocity error using explicit")
p.xlabel("Time (s)")
p.ylabel("Error (m)") 
p.plot(euler[0], velocity_error)
p.savefig("velocity_error.png")
#p.show()

# Relationship between Error and h for the explicit method
x_max = []
v_max = []
h_s = []
h = 0.1
for i in range(4):
	factor = float(2**(i+1))
	h = h/factor
	h_s.append(h)
	eu = euler_spring(1, 1, 1, 1, h, 13.35)
	ex = exact_spring(1, 1, h, 13.35)
	x = np.max(ex[1] - eu[1])
	v = np.max(ex[2] - eu[2])
	x_max.append(x)
	v_max.append(v)
p.title("Position Error vs. time step using explicit")
p.xlabel("Time step (s)")
p.ylabel("Max Error (m)") 
p.plot(h_s, x_max)
p.savefig("position_dt.png")
#p.show()
p.title("Velocity Error vs. time step using explicit")
p.xlabel("Time step (S)")
p.ylabel("Max Error (m)") 
p.plot(h_s, v_max)
p.savefig("velocity_dt.png")
#p.show()

# Energy for explicit
E_euler = euler[1]**2 + euler[2]**2
p.title("Normalized Energy vs. Time using explicit")
p.xlabel("Time (s)")
p.ylabel("Energy (J)") 
p.plot(euler[0], E_euler)
p.savefig("energy.png")
#p.show()

# Error as a function of time using implicit
position_error_im = exact[1] - euler_im[1]
velocity_error_im = exact[2] - euler_im[2]
p.title("Position error using implicit")
p.xlabel("Time (s)")
p.ylabel("Error (m)") 
p.plot(euler_im[0], position_error_im)
p.savefig("position_error_im.png")
#p.show()
p.title("Velocity error using implicit")
p.xlabel("Time (s)")
p.ylabel("Error (m)") 
p.plot(euler_im[0], velocity_error_im)
p.savefig("velocity_error_im.png")
#p.show()

# Energy for implicit
E_euler_im = euler_im[1]**2 + euler_im[2]**2
p.title("Normalized Energy vs. Time using implicit")
p.xlabel("Time (s)")
p.ylabel("Energy (J)") 
p.plot(euler_im[0], E_euler_im)
p.savefig("energy_im.png")
#p.show()

f = open('assignment4.txt', 'w')
f.write("Program has executed and completed, and produced the plots.")
